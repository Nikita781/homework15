#include <iostream>

void FindEvenNumbers(int Limit)
{
    int HalfLimit = Limit / 2;

    std::cout << "Even numbers: ";

    for (int i = 0; i <= HalfLimit; i++)
    {
        std::cout << i * 2 << " ";
    }

    std::cout << "\n";
}

void FindOddOrEvenNumber(int Limit, bool IsOdd)
{
    int HalfLimit = Limit / 2;

    if (IsOdd == true)
    {
        std::cout << "Odd numbers: ";

        for (int i = 0; i < HalfLimit; i++)
        {
            std::cout << i * 2 + 1 << " ";
        }

        std::cout << "\n";
    }
    else
    {
        FindEvenNumbers(Limit);
    }

}

int main()
{
    std::cout << "Input number: ";

    int Limit = 20;
    bool IsOdd = true;

    std::cout << Limit << "\n";

    FindEvenNumbers(Limit);
    FindOddOrEvenNumber(Limit, IsOdd);
}


